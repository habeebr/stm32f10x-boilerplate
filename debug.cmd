@echo --------------------------------------------------------------------------
@echo  Starting Debug interface for STM32VL-Discovery... %DATE% %TIME%         
@echo --------------------------------------------------------------------------
@openocd-x64-0.7.0.exe -f stm32vldiscovery.cfg
@echo --------------------------------------------------------------------------
@echo  Debug session ended                               %DATE% %TIME%        
@echo --------------------------------------------------------------------------
@REM pause