#
#       !!!! Do NOT edit this makefile with an editor which replace tabs by spaces !!!!   
#
##############################################################################################
#
# On command line:
#
# make all = Create project
#
# make clean = Clean project files.
#
# To rebuild project do "make clean" and "make all".
#
# Included originally in the yagarto projects. Original Author : Michael Fischer
# Modified to suit our purposes by Hussam Al-Hertani
# Use at your own risk!!!!!

# Define project name and Ram/Flash mode here
PROJECT        = STM32_Standart
RUN_FROM_FLASH = 1

##############################################################################################
# Start of default section
#
TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary -S
MCU  = cortex-m3
LINKERFILE = stm32f100rb.ld

# List all default C defines here, like -D_DEBUG=1
DDEFS = -DSTM32F10X_MD_VL -DUSE_STDPERIPH_DRIVER
# List all default ASM defines here, like -D_DEBUG=1
DADEFS =
 
# List all default directories to look for include files here
DINCDIR =
 
# List the default directory to look for the libraries here
DLIBDIR =
 
# List all default libraries here
DLIBS =
 
#
# End of default section
##############################################################################################
 
##############################################################################################
# Start of user section
#
 
# List all user C define here, like -D_DEBUG=1
UDEFS =
 
# Define ASM defines here
UADEFS =
 
# List C source files here
LIBSDIR    = .
CORELIBDIR = $(LIBSDIR)\CMSIS
STMSPDDIR    = $(LIBSDIR)\StdPeripheralDriver
STMSPSRCDDIR = $(STMSPDDIR)\src
STMSPINCDDIR = $(STMSPDDIR)\inc
FREERTOS = .\FreeRTOS
FREERTOSSRC = $(FREERTOS)\Source
FREERTOSSRCINC = $(FREERTOSSRC)\include
FREERTOSSRCPORT = $(FREERTOSSRC)\portable
OUTDIR = .\out
LINKER = .\linker
$(shell mkdir $(OUTDIR))

## Source Code
SRC  = .\src\main.c
#SRC += .\src\stm32f10x_it.c
SRC += $(CORELIBDIR)\system_stm32f10x.c
SRC += $(CORELIBDIR)\startup_stm32f10x_md_vl.c
## used parts of the STM-Library
SRC += $(STMSPSRCDDIR)\stm32f10x_rcc.c
SRC += $(STMSPSRCDDIR)\stm32f10x_gpio.c
SRC += $(STMSPSRCDDIR)\misc.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_adc.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_bkp.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_can.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_cec.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_crc.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_dac.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_dbgmcu.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_dma.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_exti.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_flash.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_fsmc.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_i2c.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_iwdg.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_pwr.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_rtc.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_sdio.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_spi.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_tim.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_usart.c
#SRC += $(STMSPSRCDDIR)\stm32f10x_wwdg.c
##FreeRTOS source files here
#SRC += $(FREERTOSSRC)\list.c
#SRC += $(FREERTOSSRC)\queue.c
#SRC += $(FREERTOSSRC)\tasks.c
#SRC += $(FREERTOSSRC)\timers.c
#SRC += $(FREERTOSSRCPORT)\heap_1.c
#SRC += $(FREERTOSSRCPORT)\heap_2.c
#SRC += $(FREERTOSSRCPORT)\port.c
# List ASM source files here
# ASRC = $(DEVDIR)\startup_stm32f10x_md_vl.S
 
# List all user directories here
UINCDIR = $(CORELIBDIR) \
          $(STMSPINCDDIR) \
          $(FREERTOS) \
          $(FREERTOSSRCINC) \
          $(FREERTOSSRCPORT) \
          .\inc     
# List the user directory to look for the libraries here
ULIBDIR =
 
# List all user libraries here
ULIBS =
 
# Define optimisation level here
OPT = -Os
 
#
# End of user defines
##############################################################################################
#
# Define linker script file here
#
ifeq ($(RUN_FROM_FLASH), 0)
LDSCRIPT = $(LINKER)\$(LINKERFILE)
FULL_PRJ = $(OUTDIR)\$(PROJECT)_ram
else
LDSCRIPT = .\linker\$(LINKERFILE)
FULL_PRJ = $(OUTDIR)\$(PROJECT)_rom
endif
 
INCDIR  = $(patsubst %,-I%,$(DINCDIR) $(UINCDIR))
LIBDIR  = $(patsubst %,-L%,$(DLIBDIR) $(ULIBDIR))
 
ifeq ($(RUN_FROM_FLASH), 0)
DEFS    = $(DDEFS) $(UDEFS) -DRUN_FROM_FLASH=0 -DVECT_TAB_SRAM
else
DEFS    = $(DDEFS) $(UDEFS) -DRUN_FROM_FLASH=1
endif
 
ADEFS   = $(DADEFS) $(UADEFS)
OBJS  = $(ASRC:.S=.o) $(SRC:.c=.o)
LIBS    = $(DLIBS) $(ULIBS)
MCFLAGS = -mcpu=$(MCU)
 
ASFLAGS = $(MCFLAGS) -g -gdwarf-2 -mthumb  -Wa,-amhls=$(<:.s=.lst) $(ADEFS)
CPFLAGS = $(MCFLAGS) $(OPT) -gdwarf-2 -mthumb   -fomit-frame-pointer -Wall -fverbose-asm -Wa,-ahlms=$(<:.c=.lst) $(DEFS)
#CPFLAGS = $(MCFLAGS) $(OPT) -gdwarf-2 -mthumb   -fomit-frame-pointer -Wall -Wstrict-prototypes -fverbose-asm -Wa,-ahlms=$(<:.c=.lst) $(DEFS)
LDFLAGS = $(MCFLAGS) -mthumb -nostartfiles -T$(LDSCRIPT) -Wl,-Map=$(FULL_PRJ).map,--cref,--no-warn-mismatch $(LIBDIR)
 
# Generate dependency information
CPFLAGS += -MD -MP -MF .dep\$(@F).d
 
#
# makefile rules
#
 
all: $(OBJS) $(FULL_PRJ).elf  $(FULL_PRJ).hex $(FULL_PRJ).bin
	$(TRGT)size $(FULL_PRJ).elf
#ifeq ($(RUN_FROM_FLASH), 0)
#	$(TRGT)size $(PROJECT)_ram.elf
#else
#	$(TRGT)size $(PROJECT)_rom.elf
#endif
 
 %o: %c
	$(CC) -c $(CPFLAGS) -I . $(INCDIR) $< -o $@

%o: %s
	$(AS) -c $(ASFLAGS) $< -o $@

%elf: $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) $(LIBS) -o $@

%hex: %elf
	$(HEX) $< $@
	
%bin: %elf
	$(BIN)  $< $@
	
clean:
	del $(OBJS)
	del $(FULL_PRJ).elf
	del $(FULL_PRJ).map
	del $(FULL_PRJ).hex
	del $(FULL_PRJ).bin
#	del $(SRC:.c=.c.bak)
	del $(SRC:.c=.lst)
#   del $(ASRC:.s=.s.bak)
#	del $(ASRC:.S=.lst)
	rmdir out /Q
	rmdir  .dep /S /Q
    
    
# 
# Include the dependency files, should be the last of the makefile
#
#-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)
#23-July-2012 /dev/null gives an error on Win 7 64-bit : Hussam
-include $(shell mkdir .dep) $(wildcard .dep/*)

# *** EOF ***